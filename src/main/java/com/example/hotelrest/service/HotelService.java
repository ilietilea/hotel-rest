package com.example.hotelrest.service;

import com.example.hotelrest.model.CurrentWeather;
import com.example.hotelrest.model.Hotel;
import com.example.hotelrest.model.HotelDto;
import com.example.hotelrest.model.WeatherResponse;
import com.example.hotelrest.repository.HotelRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HotelService {

  private final HotelRepository hotelRepository;
  private final WeatherService weatherService;

  public Hotel addHotel(HotelDto hotelDto) {
    return hotelRepository.save(Hotel.from(hotelDto));
  }

  public Hotel getHotel(Integer id) {
    Hotel hotel = hotelRepository.findById(id)
        .orElseThrow(() -> new RuntimeException("Hotel not found"));
    WeatherResponse weatherResponse = weatherService.getWeather(hotel.getCity());
    CurrentWeather currentWeather = weatherResponse == null ? null
        : new CurrentWeather(weatherResponse.getWeatherMain().getTemp(),
            weatherResponse.getWeather().get(0).getDescription());
    hotel.setWeather(currentWeather);
    return hotel;
  }

  public List<Hotel> getHotels() {
    return hotelRepository.findAll();
  }

  public Hotel updateHotel(Integer id, HotelDto hotelDto) {
    Hotel hotel = hotelRepository.findById(id)
        .orElseThrow(() -> new RuntimeException("Hotel not found"));
    hotel.setCity(hotelDto.getCity());
    hotel.setCountry(hotelDto.getCountry());
    hotel.setName(hotelDto.getName());
    hotel.setPricePerNight(hotelDto.getPricePerNight());
    return hotelRepository.save(hotel);
  }

  public void deleteHotel(Integer id) {
    hotelRepository.deleteById(id);
  }
}
