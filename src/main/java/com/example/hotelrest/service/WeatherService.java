package com.example.hotelrest.service;

import com.example.hotelrest.model.WeatherResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class WeatherService {

  private final RestTemplate restTemplate;

  private String url = "http://api.openweathermap.org/data/2.5/weather?q={city}&appid={appId}&units=metric&lang=ro";
  private String appId = "c88ebac2c68b2ed507850a0dc2d93ac2";

  public WeatherResponse getWeather(String city) {
    return restTemplate.getForObject(url, WeatherResponse.class, city, appId);
  }
}
