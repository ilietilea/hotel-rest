package com.example.hotelrest.exception;

import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class ApiExceptionHandler {

  @ExceptionHandler
  public ResponseEntity<ExceptionResponse> handleValidationExceptions(
      final MethodArgumentNotValidException ex) {
    log.error(ex.getMessage(), ex);
    String errorMessage = ex.getBindingResult().getAllErrors().get(0).getDefaultMessage();
    return new ResponseEntity<>(
        new ExceptionResponse(LocalDateTime.now(), "validationError", errorMessage),
        HttpStatus.BAD_REQUEST);
  }
}
