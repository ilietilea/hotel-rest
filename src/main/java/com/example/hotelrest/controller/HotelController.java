package com.example.hotelrest.controller;

import com.example.hotelrest.model.Hotel;
import com.example.hotelrest.model.HotelDto;
import com.example.hotelrest.service.HotelService;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequiredArgsConstructor
public class HotelController {

  private final HotelService hotelService;
  private final RestTemplate restTemplate;

  @PostMapping("/hotel")
  public ResponseEntity<Hotel> addHotel(@RequestBody @Valid HotelDto hotelDto) {
    return new ResponseEntity<>(hotelService.addHotel(hotelDto), HttpStatus.CREATED);
  }

  @GetMapping("/hotel/{id}")
  public Hotel getHotel(@PathVariable Integer id) {
    return hotelService.getHotel(id);
  }

  @GetMapping("/hotel")
  public List<Hotel> getHotels() {
    return hotelService.getHotels();
  }

  @PutMapping("/hotel/{id}")
  public Hotel updateHotel(@PathVariable Integer id, @RequestBody HotelDto hotelDto) {
    return hotelService.updateHotel(id, hotelDto);
  }

  @DeleteMapping("/hotel/{id}")
  public ResponseEntity<Void> deleteHotel(@PathVariable Integer id) {
    hotelService.deleteHotel(id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
