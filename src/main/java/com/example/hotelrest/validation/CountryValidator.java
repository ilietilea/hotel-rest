package com.example.hotelrest.validation;

import com.example.hotelrest.model.HotelDto;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CountryValidator implements ConstraintValidator<ValidCountry, HotelDto> {

  @Override
  public boolean isValid(HotelDto hotelDto, ConstraintValidatorContext context) {
    String country = hotelDto.getCountry();
    return !country.equals("Afganistan");
  }
}
