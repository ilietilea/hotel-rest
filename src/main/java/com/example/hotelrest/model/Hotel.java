package com.example.hotelrest.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class Hotel {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private UUID uuid;
  private String name;
  private Integer stars;
  private String country;
  private String city;
  private Double pricePerNight;
  @Transient
  private CurrentWeather weather;

  public static Hotel from(HotelDto hotelDto) {
    return builder()
        .uuid(UUID.randomUUID())
        .name(hotelDto.getName())
        .stars(hotelDto.getStars())
        .city(hotelDto.getCity())
        .country(hotelDto.getCountry())
        .pricePerNight(hotelDto.getPricePerNight())
        .build();
  }
}
