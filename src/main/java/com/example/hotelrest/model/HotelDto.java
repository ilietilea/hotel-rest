package com.example.hotelrest.model;

import com.example.hotelrest.validation.ValidCountry;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@ValidCountry(message = "Invalid country")
public class HotelDto {

  @NotNull
  private String name;
  @Min(value = 1, message = "Hotel should have at least 1 star")
  @Max(value = 5, message = "Hotel should have max 5 stars")
  private Integer stars;
  @NotEmpty
  private String country;
  private String city;
  private Double pricePerNight;

}
